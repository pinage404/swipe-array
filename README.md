# \<swipe-array\>

[![Pipeline status][pipeline-badge]][pipeline-url]
[![Published on NPM][npm-badge]][npm-url]

Element that render a template with a array in input


- [Install](#install)
    - [Install with Yarn](#install-with-yarn)
    - [Install with NPM](#install-with-npm)
        - [Simple install](#simple-install)
        - [Manual install](#manual-install)
    - [Install with Bower](#install-with-bower)
- [Usage](#usage)
- [Viewing element](#viewing-element)
- [Running tests](#running-tests)
- [Licence](#licence)


## Install

### Install with Yarn

Using [Yarn][yarn]

```sh
$ yarn add swipe-array
```


### Install with NPM

#### Simple install

```sh
$ npm install swipe-array --save
```


#### Manual install

First, make sure you have [Bower][bower] and [Polymer CLI][polymer-cli] installed

```sh
$ npm install swipe-array --no-optional --save
```


### Install with Bower

First, make sure you have [Bower][bower] and [Polymer CLI][polymer-cli] installed

```sh
$ npm install fixed-size-circular-array --save
$ bower install swipe-array --save
$ npm run-script analyze # if you need to watch the documentation
```


## Usage

`<swipe-array>` need an array by filling `items` attribute and a `<template>` to know how to render element

<!--
```html
<custom-element-demo>
	<template>
		<link rel="import" href="swipe-array.html" />
		<next-code-block></next-code-block>
		<custom-style>
			<style is="custom-style">
				swipe-array {
					min-height: 3em;
					color: white;

					--swipe-array-elements: {
						padding: 1em;
					};
				}
			</style>
		</custom-style>
	</template>
</custom-element-demo>
```
-->

```html
<swipe-array
	turn
	swipeable
	items='[ "royalblue", "darkgreen", "purple", "mediumslateblue", "maroon" ]'
>
	<template>
		<div style="background-color: [[item]];">[[item]]</div>
	</template>
</swipe-array>
```


## Viewing element

```sh
$ npm start
```


## Running tests

To run tests, you must install NPM `devDependencies`

```sh
$ npm test
```


## Licence

[MIT][licence]



[pipeline-badge]: https://gitlab.com/pinage404/swipe-array/badges/master/pipeline.svg
[pipeline-url]:   https://gitlab.com/pinage404/swipe-array/commits/master

[npm-badge]: https://img.shields.io/npm/v/swipe-array.svg
[npm-url]:   https://www.npmjs.com/package/swipe-array

[yarn]: https://yarnpkg.com

[bower]:       https://bower.io/#install-bower
[polymer-cli]: https://www.npmjs.com/package/polymer-cli

[licence]: ./LICENCE
