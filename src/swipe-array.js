/**
 * __`<swipe-array>` focus an item of an "infinite" array__
 * 
 * It __need a template__ to know how to render the focused element with the selected item
 * 
 * 
 * ### Height
 * 
 * Rendered element fit the `<swipe-array>` size, this is why the height must be fixed:
 * - by setting CSS `height` nor `min-height` property
 * - by setting parent element CSS `display` property with `flex` or `grid`
 * - or any other method that force the height
 * 
 * 
 * ### Friendly performances
 * 
 * To keep correct _performances_ with large array, there are maximum _5 items rendered at a time_
 * 
 * 
 * ### Differences between `<iron-list>`
 * 
 * [`<iron-list>`][iron-list] and `<swipe-array>` are not made for the same purpose
 * 
 * `<iron-list>` is made to be a [`material design list`][material-design-list]
 * > Lists present multiple line items vertically as a single continuous element.
 * 
 * `<swipe-array>` is horizontal, it look like a "carousel" for an "infinite" array
 * 
 * 
 * ### Styling
 * 
 * `<swipe-array>` provides the following mixins for styling:
 * 
 * Custom property                  | Description                           | Default
 * :--------------------------------|:--------------------------------------|:-------
 * `--swipe-array-insert`           | Mixin applied to the insert element   | `{}`
 * `--swipe-array-elements`         | Mixin applied to all stamped elements | `{}`
 * `--swipe-array-first-element`    | Mixin applied to the first element    | `{}`
 * `--swipe-array-previous-element` | Mixin applied to the previous element | `{}`
 * `--swipe-array-selected-element` | Mixin applied to the selected element | `{}`
 * `--swipe-array-next-element`     | Mixin applied to the next element     | `{}`
 * `--swipe-array-last-element`     | Mixin applied to the last element     | `{}`
 * 
 * [iron-list]: https://www.webcomponents.org/element/PolymerElements/iron-list
 * [material-design-list]: https://material.io/guidelines/components/lists.html
 * 
 * @customElement
 * @polymer
 * @appliesMixin PoolRenderMixin
 * @demo ./demo/
 */
class SwipeArray extends PoolRenderMixin(Polymer.Element) {
	static get is() {
		return 'swipe-array'
	}


	static get properties() {
		return {
			/** An array with the data who are given to the template */
			items: {
				type: Array,
				value: [],
			},

			/** The name of the alias who are be given to the template */
			as: {
				type: String,
				value: 'item',
			},

			/** The name of the index who are given to the template */
			indexAs: {
				type: String,
				value: 'index',
			},

			/** The item in `items` currently selected who are given to the focused element */
			selected: {
				type: Object,
				notify: true,
			},

			/**
			 * When `turn === false`
			 * - `previous()` does nothing when the first index is focused
			 * - `next()` does nothing when the last index is focused
			 * 
			 * When `turn === true`
			 * - `previous()` focus the last index when the first index is focused
			 * - `next()` focus the first index when the last index is focused
			 */
			turn: {
				type: Boolean,
				value: false,
			},

			/** Internal `selectedIndex` */
			__selectedIndex: {
				type: Number,
				value: 0,
			},

			/** Allow to swipe the screen to focus previous and next item */
			swipeable: {
				type: Boolean,
				value: false,
				observer: '__listenSwipe',
			},
			/**
			 * Swiping under this limit has no effect
			 * 
			 * _This prevents accidental move while scrolling vertically_
			 * 
			 * Value are in pixel (`px`)
			 */
			swipeLimitTriggerMove: {
				type: Number,
				value: 50,
			},
			/**
			 * When the swipe go further than this limit, it focus previous or next element
			 * 
			 * Value are in pixel (`px`)
			 */
			swipeLimitTriggerAction: {
				type: Number,
				value: 150,
			},
			/** This attribute is set while swipping has begun */
			swiping: {
				type: Boolean,
				value: false,
				readOnly: true,
				reflectToAttribute: true,
			},
		}
	}

	static get observers() {
		return [
			'__deletePoolIfNeeded(items)',
			// order matters : `selected` is more important than `selectedIndex`
			'__updateSelectedIndex(items, selected)',
			'__updateSelected(items, __selectedIndex)',
			'__renderDebounced(items, __selectedIndex, as, indexAs)',
		]
	}


	constructor() {
		super()

		Polymer.Gestures.setTouchAction(this, 'pan-y pinch-zoom')
	}


	get slotElement() {
		return this.$.slot
	}

	get insertElement() {
		return this.$.insert
	}


	/**
	 * Limits the `index` between 0 and `items.length - 1`
	 * 
	 * If `turn === true` and if `index` is outside of the limits, it will loop to be inside the limits
	 * 
	 * @param {Number} index
	 * @return {Number}
	 */
	__circularIndex(index) {
		index = index || 0
		index >>= 0
		if (this.turn) {
			index = circularIndex.circularIndex(index, this.items.length)
		}
		else {
			index = Math.max(index, 0)
			index = Math.min(index, Math.max(this.items.length - 1, 0))
		}
		return index
	}


	get selectedIndex() {
		return this.__selectedIndex || 0
	}
	set selectedIndex(selectedIndex) {
		const newIndex = this.__circularIndex(selectedIndex)
		if (this.__selectedIndex !== newIndex) {
			this.__selectedIndex = newIndex
		}
	}


	/**
	 * Focus previous element
	 * 
	 * @return {SwipeArray}
	 */
	previous() {
		this.selectedIndex -= 1
		return this
	}

	/**
	 * Focus next element
	 * 
	 * @return {SwipeArray}
	 */
	next() {
		this.selectedIndex += 1
		return this
	}


	__updateSelectedIndex(items=[], selected) {
		if (typeof selected !== 'undefined') {
			const selectedIndex = items.indexOf(selected)
			if (this.selectedIndex !== selectedIndex && selectedIndex !== -1) {
				this.selectedIndex = selectedIndex
			}
		}
	}

	__updateSelected(items=[], selectedIndex) {
		if (typeof selectedIndex !== 'undefined' && this.selected !== items[selectedIndex]) {
			this.selected = items[selectedIndex]
		}
	}


	__listenSwipe(isSwipeable) {
		if (isSwipeable) {
			Polymer.Gestures.addListener(this, 'track', this.__swipeEventHandler)
		}
		else {
			Polymer.Gestures.removeListener(this, 'track', this.__swipeEventHandler)
		}
	}


	__swipeEventHandler({detail: { state, dx } }) {
		if (state === 'start') {
			this._setSwiping(true)
		}
		else if (state === 'track') {
			// this condition prevents accidental move while scrolling vertically
			if (Math.abs(dx) > this.swipeLimitTriggerMove) {
				this.insertElement.style.transform = `translateX(${dx}px)`
			}
			else {
				// reset position ; usefull after moves > swipeLimitTriggerMove then moves < swipeLimitTriggerMove
				this.insertElement.style.transform = ''
			}
		}
		else if (state === 'end') {
			if (Math.abs(dx) > this.swipeLimitTriggerAction) {
				if (dx > 0) {
					this.previous()
				}
				else {
					this.next()
				}
			}

			this.insertElement.style.transform = ''
			this._setSwiping(false)
		}
	}
}

window.customElements.define(SwipeArray.is, SwipeArray)
